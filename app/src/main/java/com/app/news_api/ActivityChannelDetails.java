package com.app.news_api;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.app.news_api.adapter.AdapterNewsListWithHeader;
import com.app.news_api.data.Constant;
import com.app.news_api.data.IResult;
import com.app.news_api.data.Tools;
import com.app.news_api.data.VolleyService;
import com.app.news_api.model.Articles;
import com.app.news_api.model.NewsSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityChannelDetails extends AppCompatActivity {
    public static final String EXTRA_OBJCT = "com.app.news_api.SOURCE";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, NewsSource obj) {
        Intent intent = new Intent(activity, ActivityChannelDetails.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Toolbar toolbar;
    private ActionBar actionBar;
    private AdapterNewsListWithHeader mAdapter;
    private NewsSource source;
    private View parent_view;
    private RecyclerView recyclerView;

    IResult mResultCallback = null;
    VolleyService mVolleyService;
    //Creating a List of articles
    private List<Articles> listArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_details);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(android.R.id.content), EXTRA_OBJCT);

        // get extra object
        source = (NewsSource) getIntent().getSerializableExtra(EXTRA_OBJCT);

        initToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //Initializing our list
        listArticles = new ArrayList<>();

        getArticlesVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback,this);
        mVolleyService.getDataStringRequest("GET",Constant.ARTICLES_URL+"?source="+source.getId()+"&apiKey="+ Constant.NEWS_API_KEY);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(source.getName());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    // testing volley service
    void getArticlesVolleyCallback(){

        mResultCallback = new IResult() {
            @Override
            public void stringRequestNotifySuccess(String requestType,String response) {
                String status = null;
                JSONObject jsonObjRes;

                try {
                    jsonObjRes = new JSONObject(response);
                    status = jsonObjRes.getString("status");
                    if(status.equals("ok")){
                        // source is json array
                        JSONArray articlesArray = jsonObjRes.getJSONArray("articles");
                        //Parse the json articles response
                        parseData(articlesArray);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                if((error instanceof NetworkError) || (error instanceof NoConnectionError)){
                    Snackbar.make(parent_view, "There is no internet connection!!!", Snackbar.LENGTH_SHORT).show();
                }else if((error instanceof ServerError)){
                    Snackbar.make(parent_view, "There are no artcles for "+source.getName(), Snackbar.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(parent_view, error.toString(), Snackbar.LENGTH_SHORT).show();
                }
            }
        };
    }

    //This method will parse json data
    private void parseData(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {
            //Creating the object
            Articles article = new Articles();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
                //Adding data to the object
                article.setAuthor(json.getString("author"));
                article.setTitle(json.getString("title"));
                article.setDescription(json.getString("description"));
                article.setUrl(json.getString("url"));
                article.setUrlToImage(json.getString("urlToImage"));
                article.setPublishedAt(json.getString("publishedAt"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Adding the object to the list
            listArticles.add(article);
        }

        //Notifying the adapter that data has been added or changed
        displayListNews(listArticles);

//        mAdapter.notifyDataSetChanged();
    }

    private void displayListNews(List<Articles> listArticles) {
        if(listArticles.isEmpty())
        {
            // Do something with the empty list here.
        }else{

        //set data and list adapter
            mAdapter = new AdapterNewsListWithHeader(this, listArticles.get(listArticles.size()-1), listArticles);
            recyclerView.setAdapter(mAdapter);

            mAdapter.setOnItemClickListener(new AdapterNewsListWithHeader.OnItemClickListener() {
                @Override
                public void onItemClick(View v, Articles obj, int position) {
                    ActivityNewsDetails.navigate(ActivityChannelDetails.this, v.findViewById(R.id.image), obj);
                }
            });
        }
    }
}
