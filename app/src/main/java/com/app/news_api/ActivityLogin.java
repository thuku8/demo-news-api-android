package com.app.news_api;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.news_api.data.BaseActivity;
import com.app.news_api.data.Tools;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by shobo on 9/6/17.
 */

public class ActivityLogin extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "ActivityLogin";


    private Button btn_signin;
    private TextView registerLink;
    private EditText mUsernameField;
    private EditText mPasswordField;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        hideKeyboard();

        btn_signin = (Button) findViewById(R.id.btn_signin);
        registerLink = (TextView) findViewById(R.id.registerLink);
        mUsernameField = (EditText) findViewById(R.id.input_username);
        mPasswordField = (EditText) findViewById(R.id.input_password);
        btn_signin.setOnClickListener(this);
        registerLink.setOnClickListener(this);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    private void signIn(String username, String password) {

        if (!validateForm()) {
            return;
        }

        //for now we can only use emails to login
        showProgressDialog();

        // [START sign_in_with]
        mAuth.signInWithEmailAndPassword(username+"@news-api.com", password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
//                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
//                            Log.e(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Authentication failed.",Toast.LENGTH_SHORT).show();
                            updateUI(null);
                            mPasswordField.setText("");
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                            Toast.makeText(ActivityLogin.this, "Sorry!!!Authentication failed!!!",Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with]
    }

    private boolean validateForm() {
        boolean valid = true;

        String username = mUsernameField.getText().toString();
        if (TextUtils.isEmpty(username)) {
            mUsernameField.setError("Required.");
            valid = false;
        } else {
            if(username.length() < 4){
                mUsernameField.setError("4 or more characters...");
                valid = false;
            }else{
                mUsernameField.setError(null);
            }
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            if(password.length() < 6){
                mPasswordField.setError("6 or more characters...");
                valid = false;
            }else{
                mPasswordField.setError(null);
            }
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            //this means the user is logged in
            Toast.makeText(ActivityLogin.this, "Welcome "+user.getDisplayName()+" .Discover best news around the world!!!",Toast.LENGTH_SHORT).show();

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
                    Intent i = new Intent(ActivityLogin.this,ActivityMain.class);
                    startActivity(i);
                    finish();
//                }
//            }, 2500);


        } else {
            //this means you are signed out
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_signin:
                signIn(mUsernameField.getText().toString(), mPasswordField.getText().toString());
                break;
            case R.id.registerLink:
                Intent iR = new Intent(ActivityLogin.this,ActivityRegister.class);
                startActivity(iR);
            default:
                break;
        }

    }
}
