package com.app.news_api.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.app.news_api.ActivityChannelDetails;
import com.app.news_api.ActivityMain;
import com.app.news_api.R;
import com.app.news_api.adapter.AdapterSourcesList;
import com.app.news_api.data.Constant;
import com.app.news_api.data.IResult;
import com.app.news_api.data.VolleyService;
import com.app.news_api.model.NewsSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by shobo on 9/6/17.
 */

public class FragmentAllSources extends Fragment {

    private String TAG = "FragmentAllSources";

    private View root_view;
    private RecyclerView recyclerView;
    private AdapterSourcesList mAdapter;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    private Context context;

    //Creating a List of sources
    private List<NewsSource> listAllSources;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_all_sources, null);

        context=getActivity();

        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        //Initializing our sources list
        listAllSources = new ArrayList<>();

        // Init volley service
        getAllSourcesVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback,getActivity());
        mVolleyService.getDataStringRequest("GET", Constant.ALL_SOURCES_URL);

        //set data and list adapter
        mAdapter = new AdapterSourcesList(getActivity(), listAllSources);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new AdapterSourcesList.OnItemClickListener() {
            @Override
            public void onItemClick(View v, NewsSource obj, int position) {
                ActivityChannelDetails.navigate((ActivityMain)getActivity(), v, obj);
            }
        });
        return root_view;
    }


    // testing volley service
    void getAllSourcesVolleyCallback(){
        mResultCallback = new IResult() {

            @Override
            public void stringRequestNotifySuccess(String requestType,String response) {

                String status = null;
                JSONObject jsonObjRes;

                try {
                    jsonObjRes = new JSONObject(response);
                    status = jsonObjRes.getString("status");
                    if(status.equals("ok")){
                        // source is json array
                        JSONArray sourcesArray = jsonObjRes.getJSONArray("sources");
                        //Parse the json sources response
                        parseData(sourcesArray);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType,VolleyError error) {
                if((error instanceof NetworkError) || (error instanceof NoConnectionError)){
                    Snackbar.make(root_view, "There is no internet connection!!!", Snackbar.LENGTH_SHORT).show();
                }else if((error instanceof ServerError)){
                    Snackbar.make(root_view, "There are no sources!!!", Snackbar.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(root_view, error.toString(), Snackbar.LENGTH_SHORT).show();
                }
            }
        };
    }



    //This method will parse json data
    private void parseData(JSONArray array) {

        //random icons and color
        TypedArray drw_arr = context.getResources().obtainTypedArray(R.array.channel_icon);
        String color_arr[] = context.getResources().getStringArray(R.array.channel_color);


        for (int i = 0; i < array.length(); i++) {
            //Creating the object
            NewsSource source = new NewsSource();
            JSONObject json = null;
            try {

                int image = (int) (Math.random() * drw_arr.length());
                int idx = new Random().nextInt(color_arr.length);
                //Getting json
                json = array.getJSONObject(i);
                //Adding data to the object
                source.setId(json.getString("id"));
                source.setName(json.getString("name"));
                source.setDescription(json.getString("description"));
                source.setUrl(json.getString("url"));
                source.setCategory(json.getString("category"));
                source.setLanguage(json.getString("language"));
                source.setCountry(json.getString("country"));
                source.setColor(color_arr[idx]);
                source.setIcon(drw_arr.getResourceId(image, -1));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //important but can be sorted later
//            drw_arr.recycle();
            //Adding the object to the list
            listAllSources.add(source);
        }

        //Notifying the adapter that data has been added or changed
        mAdapter.notifyDataSetChanged();
    }
}
