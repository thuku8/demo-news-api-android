package com.app.news_api.data;

import com.android.volley.VolleyError;

/**
 * Created by shobo on 9/6/17.
 */
public interface IResult {
    public void stringRequestNotifySuccess(String requestType, String response);
    public void notifyError(String requestType, VolleyError error);
}
