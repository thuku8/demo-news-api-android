package com.app.news_api.data;

import android.app.Application;
import android.content.Context;
import android.os.SystemClock;
import android.support.multidex.MultiDex;

import java.util.concurrent.TimeUnit;

public class GlobalVariable extends Application {

    private static String TAG = "Global";
    /*
        ****If you have faced a problem with Firebase when run application below API 19(< 4.4.2)
        ****devices due to error of Multidex. Then below solution work for me:
    */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Don't do this! This is just so cold launches take some time
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(0));
    }
}
