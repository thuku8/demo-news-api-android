package com.app.news_api.data;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by shobo on 9/6/17.
 */

public class VolleyService {

    IResult mResultCallback = null;
    Context mContext;

    public VolleyService(IResult resultCallback, Context context){
        mResultCallback = resultCallback;
        mContext = context;
    }


    //this is for dealing with string requests

    public void getDataStringRequest(final String requestType, String url){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest stringRequest = new StringRequest(Request.Method.GET,url,new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //remember what is returned here is a string response and sshoulf be converted to a json object
                    //Do it with this it will work
                    //Convert the String response to JSONObject in the calling activity
                    if(mResultCallback != null)
                        mResultCallback.stringRequestNotifySuccess(requestType, response);
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null)
                        mResultCallback.notifyError(requestType, error);
                }
            });

            queue.add(stringRequest);

        }catch(Exception e){

        }
    }
}
